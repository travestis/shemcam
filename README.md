# README #

Internet Travestis and Shemales info

https://www.travestiscam.es/ver-webcam-travestis/

“La definición de locura es hacer las cosas de la misma forma una y otra vez, esperando que algo cambie algún día”  -Albert Einstein



Hoy quiero contarte algo muy personal... además de darte el tercer y úlitmo video de la serie.



Dime si me equivoco, pero voltea a tu alrededor y fíjate cómo la enorme mayoría de las personas que conoces viven en una rueda de hámster.



Corren y corren todos los días, pero nunca llegan a algún lugar en particular.



Me recuerda a un episodio de Pinky y Cerebro… (para mis lectores más jóvenes, era una caricatura de un par de ratas de laboratorio) 



…Cerebro le estaba diciendo a Pinky su plan más reciente para dominar al mundo, y Pinky, que estaba corriendo en su rueda, le responde:



“Espera, Cerebro... ¡creo que por fin estoy llegando a algún lado!”



¿Suena conocido?



Es triste cuando una persona hace, y hace, y hace cosas con la esperanza de crecer, y conseguir lo que quiere, pero sin llegar a ningún lado realmente...



...y cree que si sigue corriendo algún día esa rueda lo llevará a algún lado.



Esa es la rueda del hámster en la que están la mayoría de las personas que conoces.



Corren y corren, pero sólo se mantienen en la misma relación tóxica, en el mismo tipo de empleo que no les apasiona, con los mismos ingresos, y con los mismos amigos y círculos sociales de hace 10 años.



Ya dejaron de crecer... y sus vidas se han vuelto monótonas.



Ahora bien, si sólo nos mantuviéramos dando vueltas en el mismo lugar cómodamente, para algunos no estaría tan mal… 



…pero hay un grave peligro:



Permanecer en la rueda del hámster te lleva a las famosas “espirales de la muerte”.





Hoy te voy a enseñar qué son y cómo salir de la rueda del hamster





Cuando yo mismo estaba atorado en mi propia rueda, llegó un momento en que me empecé a cuestionar si tenía caso seguir corriendo, y estaba seriamente considerando parar.



Voy a compartir algo muy personal contigo sobre esa etapa de mi vida…



Era 2007, a mis 25 años vivía en casa de mi madre, tenía un trabajo que me aburría y me drenaba de la poca buena energía que tenía… y lo que ganaba apenas lo justificaba.



Mi definición de “amistades” eran personas que encontraban cualquier pretexto para emborracharse conmigo y quejarse de la vida.



Estaba en una relación destructiva a larga distancia, y sin suficiente autoestima para terminarla y buscar algo mejor…



…y poco sabía que todo estaba a punto de complicarse aún más…



No quiero aburrirte con detalles, y si quieres saber lo que pasó te lo contaré más adelante en otro correo…



…pero el panorama de mi vida lucía más triste que un mosquito picando a una momia.



Parecía como si los astros se hubieran alineado para que Enrique se diera un tiro.



Pero hice algo que SALVÓ mi vida y mi felicidad.



En lugar de hacer lo que todo el mundo me decía que hiciera, hice lo opuesto…



…y mi rueda de hamster se convirtió en un coche de Fórmula 1...



...y el resto es historia.





Es IMPORTANTÍSIMO que veas este último video de la serie, mientras más rápido mejor.





Hoy te voy a enseñar cómo salir de “neutral” y meterle velocidad a ese vehículo para que conviertas la rueda de hámster en un Ferrari que te lleva a donde quieres, rápidamente, y con estilo.



Hablamos pronto,



Enrique “Espirales de éxito” Delgadillo



P.D. Aviso importante para suscriptores nuevos… 



Si quieres aprender más sobre hackeo mental, reprogramación de emociones y pensamientos destructivos o negativos para poder superar y crear lo que quieres en tu vida…



…tengo algo muy especial que estamos haciendo este mes.



No quiero prometer nada, pero si aún hay tiempo, recibirás un aviso próximamente, sólo está al pendiente de tu email :)




